/**
 * We use jsonschema-tools built-in tests,
 * but it uses mocha so we override it with
 * vitest globally using the --globals flag
 */
import { tests } from "@wikimedia/jsonschema-tools";

tests.all({ logLevel: "info" });
